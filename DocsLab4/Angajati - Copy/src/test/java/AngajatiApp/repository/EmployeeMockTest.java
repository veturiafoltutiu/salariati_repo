package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    private Employee employee;
    private EmployeeMock employeeMock;

    @BeforeEach
    void setUp() {
        employee = new Employee();
        employee.setId(1);
        employeeMock = new EmployeeMock();
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    void modifyEmployeeFunctionP03() {
        //arrange
        employee.setId(20);

        //act
        employeeMock.modifyEmployeeFunction(employee, DidacticFunction.TEACHER);
        //assert
        for (Employee currentEmployee : employeeMock.getEmployeeList()) {
            if (currentEmployee.getId() == employee.getId()) {
                assert (false);
            }
        }
    }

    @Test
    void modifyEmployeeFunctionP04() {
        //arrange
        //act
        employeeMock.modifyEmployeeFunction(employee, DidacticFunction.TEACHER);
        //assert
        for(Employee currentEmployee : employeeMock.getEmployeeList())
        {
            if(currentEmployee.getId() == employee.getId())
            {
                assertEquals(currentEmployee.getFunction(),DidacticFunction.TEACHER);
            }
        }
    }

    @Test
    void modifyEmployeeFunctionP01() {
        //arrange
        employee = null;
        //act
        employeeMock.modifyEmployeeFunction(employee, DidacticFunction.TEACHER);
        //assert
        for(Employee currentEmployee : employeeMock.getEmployeeList()){
            if (currentEmployee == null) {
                assert (false);

            }
        }
    }
}

