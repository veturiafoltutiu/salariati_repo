package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    private Employee employee;
    private EmployeeMock employeeMock;
    private int numberEmployee;

    @BeforeEach
    void setUp() {
        employee=new Employee();
        employee.setId(1);
        employee.setFirstName("Maria");
        employee.setLastName("Anton");
        employee.setCnp("2710703054671");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(5000d);
        employeeMock = new EmployeeMock();
        try {
            numberEmployee = employeeMock.getEmployeeList().size();
        }
        catch(Exception e)
        {
        }
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    void addEmployeeTC1() {
        //arrange
        employee.setCnp("2710703054671");
        employee.setSalary(5000d);

        //act
        boolean valid = employeeMock.addEmployee(employee);

        //assert
        assert (valid);
    }

    @Test
    void addEmployeeTC3() {
        //arrange
        employee.setSalary(-1500d);
        //act
        boolean valid = employeeMock.addEmployee(employee);
        //assert
        assert (!valid);
    }

    @Test
    void addEmployeeTC5() {
        //arrange
        employee.setSalary(500000d);
        //act
        boolean valid = employeeMock.addEmployee(employee);
        //assert
        assert (!valid);
    }

    @Test
    void addEmployeeTC9() {
        //arrange
        employee.setCnp("4528");
        //act
        boolean valid = employeeMock.addEmployee(employee);
        //assert
        assert (!valid);
    }

    @Test
    void addEmployeeTC11() {
        //arrange
        employee.setLastName("Io");
        //act
        boolean valid = employeeMock.addEmployee(employee);
        //assert
        assert (!valid);
    }

    @Test
    void addEmployeeTC12() {
        //arrange
        employee.setLastName("Dan");
        employee.setCnp("2710703054671");
        //act
        boolean valid = employeeMock.addEmployee(employee);
        //assert
        assert (valid);
    }
}